
import 'dart:io';

class Taobin {
  int? Choose;

  Taobin() {
    //constructor
    Choose = Choose;
  }

  void showMenu_Type() {
    print("Choose Menu Category");
    print("1. Coffee");
    print("2. Tea");
    print("3. Milk, Cocoa and Caramel");
    print("4. Protein shake");
    print("5. Soda and others");
    print("6. Recommended menu");
    print("-------------------------------");
  }

  void Show_Menu(int choosemenu_type) {
    switch (choosemenu_type) {
      case 1:
        {
          print("Choose Menu Coffee");
          print("1.Latte 30 Baht ");
          print("2.Cappuccino 30 Baht");
          print("3.Matcha Latte 30 Baht");
          print("4.Americano 30 Baht");
          print("-------------------------------");

          List item = ["Latte", "Cappuccino", "Matcha", "Americano"];

          int? choose = int.parse(stdin.readLineSync()!);
          List choose_item = [
            "You choose Latte 30 Baht ",
            "You choose Cappuccino 30 Baht",
            "You choose Matcha Latte 30 Baht",
            "You choose Americano 30 Baht"
          ];
          choose_sweet(choose_item, choose);

          Payment(item, choose);

        }
        break;
      case 2:
        {
          print("Choose Menu  Tea");
          print("1.Thai Milk Tea 40 Baht ");
          print("2.Taiwanese Tea 40 Baht");
          print("3.Matcha Latte Tea 40 Baht");
          print("4.Iced Tea 40 Baht");
          print("-------------------------------");
          List item = ["Thai Milk Tea", "Taiwanese Tea", "Matcha Latte Tea", "Iced Tea"];

          int? choose = int.parse(stdin.readLineSync()!);
          List choose_item = [
            "You choose Thai Milk Tea 40 Baht",
            "You choose Taiwanese Tea 40 Baht",
            "You choose Matcha Latte Tea 40 Baht",
            "You choose Iced Tea 40 Baht"
          ];
          choose_sweet(choose_item, choose);
          Payment(item, choose);
        }
        break;
      case 3:
        {
          print("Choose Menu  Milk, Cocoa and Caramel");
          print("1.Iced Pink Milk 50 Baht ");
          print("2.Iced Caramel Cocoa 50 Baht");
          print("3 Iced Cocoa 50 Baht");
          print("4.Hot Cocoa 30 Baht");
          print("-------------------------------");
          List item = ["Iced Pink Milk", "Iced Caramel Cocoa", "Iced Cocoa", "Hot Cocoa"];

          int? choose = int.parse(stdin.readLineSync()!);
          List choose_item = [
            "You choose Iced Pink Milk 50 Baht",
            "You choose Iced Caramel Cocoa 50 Baht",
            "You choose Iced Cocoa 50 Baht",
            "You choose Hot Cocoa 30 Baht"
          ];
          choose_sweet(choose_item, choose);
          Payment(item, choose);
        }
        break;

      case 4:
        {
          print("Choose Menu  Protein shake");
          print("1.Iced Lychee 50 Baht ");
          print("2.Iced Strawberry 50 Baht");
          print("3 Iced Blueberry 50 Baht");
          print("4.Iced Sala 50 Baht");
          print("-------------------------------");
          List item = ["Iced Lychee", "Iced Strawberry", "Iced Blueberry", "Iced Sala"];

          int? choose = int.parse(stdin.readLineSync()!);
          List choose_item = [
            "You choose Iced Lychee 50 Baht",
            "You choose Iced Strawberry 50 Baht",
            "You Iced Blueberry 50 Baht",
            "You choose Iced Sala 50 Baht"
          ];
          choose_sweet(choose_item, choose);
          Payment(item, choose);
        }
        break;

      case 5:
        {
          print("Choose Menu  Soda and others");
          print("1.Iced Lychee Soda 50 Baht ");
          print("2.Iced Strawberry Soda 50 Baht");
          print("3 Iced Blueberry  Soda 50 Baht");
          print("4.Iced Sala Soda 50 Baht");
          print("-------------------------------");
          List item = ["Iced Lychee Soda", "Iced Strawberry Soda", "Iced Blueberry  Soda", "Iced Sala Soda"];

          int? choose = int.parse(stdin.readLineSync()!);
          List choose_item = [
            "You choose Iced Lychee Soda 50 Baht",
            "You choose Iced Strawberry Soda 50 Baht",
            "You choose Iced Blueberry  Soda 50 Baht",
            "You choose Iced Sala Soda 50 Baht"
          ];

          choose_sweet(choose_item, choose);
          Payment(item, choose);
        }
        break;

      case 6:
        {
          print("Choose Recommended menu");
          print("1.Latte 30 Baht");
          print("2.Iced Pink Milk 50 Baht");
          print("3 Iced Cocoa 50 Baht");
          print("4.Iced Strawberry 50 Baht");
          print("-------------------------------");
          List item = ["Latte", "Iced Pink Milk", "Iced Cocoa", "Iced Strawberry"];

          int? choose = int.parse(stdin.readLineSync()!);
          List choose_item = [
            "You choose Latte 30 Baht",
            "You choose Iced Pink Milk 50 Baht",
            "You choose Iced Cocoa 50 Baht",
            "You choose Iced Strawberry 50 Baht"
          ];
          choose_sweet(choose_item, choose);
          Payment(item, choose);
        }
        break;
    }
  }

  void Payment(List<dynamic> item, int choose) {
    print("Payment");
    print("1.Cash");
    print("2.Pay with QR");

    int? payment = int.parse(stdin.readLineSync()!);
    List choose_payment = ["Cash", "Pay with QR"];
    print("You choose " + choose_payment[payment - 1]);
    print("Wait a moment ...");
    print("Finish " + item[choose - 1] + " for you");
  }

  void choose_sweet(List<dynamic> choose_item, int choose) {
    print("You choose sweet level");
    print("1. Normal 100%");
    print("2. Less Sweet 50%");
    print("3. Less Less Sweet 25%");
    print("4. Not Sweet 0%");
    print("-------------------------------");
    List sweet = [
      "Normal Sweet 100%",
      "Less Sweet 50%",
      "Less Less Sweet 25%",
      "Not Sweet 0%"
    ];

    int? choose_sweet = int.parse(stdin.readLineSync()!);
    print(choose_item[choose - 1] + " / " + sweet[choose_sweet - 1]);
  }
}
